# GameToSpeech-unity

Find the GameToSpeech script at [`/Assets/GameToSpeech`](https://framagit.org/DavidLibeau/gametospeech-unity/-/tree/master/Assets/GameToSpeech).

Find implementations of GameToSpeech at :
- [`/Assets/3DGamekit/Scripts/Game/UI/HealthUI.cs`](https://framagit.org/DavidLibeau/gametospeech-unity/-/blob/master/Assets/3DGamekit/Scripts/Game/UI/HealthUI.cs#L55)
- [`/Assets/3DGamekit/Scripts/Game/UI/DialogueCanvasController.cs`](hhttps://framagit.org/DavidLibeau/gametospeech-unity/-/blob/master/Assets/3DGamekit/Scripts/Game/UI/DialogueCanvasController.cs#L36)
