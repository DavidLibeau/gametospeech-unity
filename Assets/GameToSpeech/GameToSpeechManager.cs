﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class GameToSpeechManager : MonoBehaviour
{

    public string gameId;
    public string channel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void sendToServer(string category, string data)
    {
        StartCoroutine(serverSend(category, data));
    }

    IEnumerator serverSend(string category, string data){
        string json = "{\"category\":\""+category+"\",\"value\":\""+data+"\"}";
        Debug.Log(json);
        UnityWebRequest www = UnityWebRequest.Get("http://api.gametospeech.com/"+gameId+"/"+channel+ "/?data="+json);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log("OK");
        }
    }
}
